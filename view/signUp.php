<?php 

    require("../model/artist.php");

    function showForm(Artist $artist){

        $user       = $artist->getUser();
        $name       = $artist->getName();
        $email      = $artist->getEmail();
        $channel    = $artist->getChannel();
        $birth      = $artist->getBirth();
        $bio        = $artist->getBio();

?>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> MOVS - Cadastre - se </title>
    
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
    
    <link href="css/half-slider.css" rel="stylesheet">
</head>

<body>

    <!-- Menu -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: #333; ">
        <div class="container">
            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    
                </button>
                <a class="navbar-brand" href="../index.html" style="color:white;text-shadow: 2px 2px 4px #000000;">Página Inicial</a>
            </div>
          
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="color:white; text-shadow: 2px 2px 4px #000000;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="../controller/loadSignUp.php">Cadastre - se</a>
                    </li>
                    <li>
						<a href="../view/sobre.html">Sobre</a>
                    </li>
                    <li>
                        <a href="../view/Fale_Conosco.html">Fale Conosco</a>
                    </li>
					<li style="align:right;">
						<a href="login.php" >Login</a>
					</li>
                </ul>
            </div>
          
        </div>
        
    </nav>
	   <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin-top: 55px;">Cadastre-se</h1>
                <p style="font-size:16px;">Aqui você diz o que faz.<br>
				Esse é o espaço para o seu cadastro,preencha o formulário</br>e aproveite e nos conte um pouco sobre você,divulgue-se da maneira</br>
				que achar melhor!!!
				</p>
				
		<form action="../controller/signUp.php" method="post" id="form" enctype="multipart/form-data">
				
			<label>Usuário:</label><input type="text" name="user" id="user" placeholder="Seu nome de Usuário" value="<?= $user?>"><br>
			<label>Nome:   </label><input type="text" name="name"     id="name"     placeholder="Seu Nome" value="<?= $name?>"><br>
			<label>Email:  </label><input type="text" name="email"    id="email"    placeholder="Seu Email" value="<?= $email?>"><br>
			
			</br></br>
			
			<p>Selecione a sua categoria</p>
			<select form="form" name="codCategory" id="codCategory" minlength="2" required="required">
				  <option value="none" checked>  Escolha...    </option>
				  <option value="1">     		 Músico        </option>
				  <option value="2">             Dançarino     </option>
				  <option value="3">             Banda         </option>
				  <option value="4">             Fotógrafo     </option>
			</select> 
			
			</br></br>
			
			<input type="file" name="profilePicture" id="profilePicture" placeholder="Foto do Perfil" required="required"
			accept="image/png, image/jpeg"> 
			
			</br></br>
			
			<label>Diga qual o seu canal do Youtube:</label><input type="text" name="channel" id="channel" placeholder="Seu Canal do Youtube" value="<?= $channel?>"><br>
			
			</br></br>
			
			<label><input type="date" name="birth" id="birth"
			placeholder="Data de Nascimento" minlength="2" required="required" value="<?= $birth?>"></label>

			</br></br>

			<label><textarea form="form" style="resize: none; height:200px" 
			placeholder="Fale um pouco sobre você..." name="bio" id="bio" minlength="2" required="required"> <?= $bio?> </textarea></label><br><br>

			
			<label>Senha:</label><input type="password" name="pass" id="pass"><br>
			<label>Repita sua Senha:</label><input type="password" name="pass2" id="pass2"><br>
			
			<br><br>
			<a><button> Voltar para Página Inicial</button></a>
            <input type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">
		</form>
            </div>
        </div>
	
	
	
	  <!-- Footer -->
        <footer style="background-color: black; ">
            <div class="row">
                <div class="col-lg-12" style="
			background-color: #333;
            text-align: center;
            font-size:11pt;
            text-shadow: 2px 2px 4px #000000;
            width:100%;
            color:Black;
            position:fixed;
            bottom:0px;
            left:0px;">
                    <p style="color:white;">Copyright &copy; Luciano Junior e Vittório Andrade 2017</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script para ativar o Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //Muda a velocidade
    })
    </script>

</body>

</html>

<?php 

    }

?>