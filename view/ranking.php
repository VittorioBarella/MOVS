<?php 

	$user = $_GET['user'];

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> MOVS - Página Inicial </title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
    
    <link href="css/half-slider.css" rel="stylesheet">
</head>

<body>

    <!-- Menu -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: #333; ">
        <div class="container">
            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                   
                </button>
                <a class="navbar-brand" href="homepage.php?user=<?=$user?>" style="color:white;text-shadow: 2px 2px 4px #000000;">Página Inicial</a>
            </div>
          
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="color:white; text-shadow: 2px 2px 4px #000000;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="../view/ranking.php?user=<?=$user?>">Ranking</a>
                    </li>
                    <li>
						<a href="../view/myProfile.php?user=<?=$user?>">Meu perfil</a>
                    </li>
                    <li>
                        <a href="../view/myFriends.php?user=<?=$user?>">Meus amigos</a>
                    </li>
                    <li>
                        <a href="../view/myPosts.php?user=<?=$user?>">Minhas postagens</a>
                    </li>
					<li style="align:right">
						<a href="../index.html" >Sair</a>
					</li>
                </ul>
            </div>

        </div>
        
    </nav>
    
    <!-- Conteúdo da Página -->
    <div class="container-fluid">

        <div class="row" style="margin-top: 60px">

            <div class="col-lg-12">

            <h3> Ranking - Top 10 </h3>

            <br> <br>

                <?php 

					require("../controller/ranking.php");

					showRanking();

				?>

            </div>

        </div>

        <!-- Footer -->
        <footer style="background-color: black; ">
            <div class="row">
                <div class="col-lg-12" style="
			background-color: #333;
            text-align: center;
            font-size:11pt;
            text-shadow: 2px 2px 4px #000000;
            width:100%;
            color:Black;
            position:fixed;
            bottom:0px;
            left:0px;">
                    <p style="color:white;">Copyright &copy; Luciano Junior e Vittório Andrade 2017</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

</body>

</html>
