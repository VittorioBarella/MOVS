<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> MOVS - Login </title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
    
    <link href="css/half-slider.css" rel="stylesheet">
</head>

<body>

    <!-- Menu -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: #333; ">
        <div class="container">
            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                  
                </button>
                <a class="navbar-brand" href="../index.html" style="color:white;text-shadow: 2px 2px 4px #000000;">Página Inicial</a>
            </div>
          
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="color:white; text-shadow: 2px 2px 4px #000000;">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="../controller/loadSignUp.php">Cadastre - se</a>
                    </li>
                    <li>
                        <a href="sobre.html">Sobre</a>
                    </li>
                    <li>
                        <a href="Fale_Conosco.html">Fale Conosco</a>
                    </li>
                    <li style="align:right;">
                        <a href="login.php" >Login</a>
                    </li>
                </ul>
            </div>
          
        </div>
        
    </nav>
		<!-- Conteúdo da Página -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1  style="margin-top: 67px; margin-left: 555px;" >Efetue seu Login</h1>
                <p style="font-size:16px;text-align: justify; margin-left: 450px;">
				</br>
				
				<form action="../controller/login.php" method="post">

				 <fieldset style="width: 500px; height: 158px; margin-left: 457px; margin-top: 65px;">
				 
				  <legend>Entre:</legend>
				  
				  Usuário: <input type="text" placeholder="Nome de usuário" name="user" id="user"><br><br>
				  
			      Senha: <input type="password"  placeholder="A senha cadastrada" name="pass" id="pass"><br><br>
				  
				  <input type="submit" value="Entrar">
				  <input type="reset" value="Limpa Dados">
				  
				 </fieldset>
				
                </form>
</p>
            </div>
        </div>

  

        <!-- Footer -->
        <footer style="background-color: black; ">
            <div class="row">
                <div class="col-lg-12" style="
			background-color: #333;
            text-align: center;
            font-size:11pt;
            text-shadow: 2px 2px 4px #000000;
            width:100%;
            color:Black;
            position:fixed;
            bottom:0px;
            left:0px;">
                    <p style="color:white;">Copyright &copy; Luciano Junior e Vittório Andrade 2017</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>


