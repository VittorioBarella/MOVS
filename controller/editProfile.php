<?php 

	require('../view/signUp.php');
	require('../model/dbConnection.php');

	$artist = new Artist();

	$artist->setUser($_POST['user']);
	$artist->setName($_POST['name']);
	$artist->setCodCategory($_POST['codCategory']);
	$artist->setProfilePicture($_FILES['profilePicture']['name']);
	$artist->setChannel($_POST['channel']);
	$artist->setBirth($_POST['birth']);
	$artist->setBio($_POST['bio']);
	$artist->setPass($_POST['pass']);
	$pass2 = $_POST['pass2'];

	if(!preg_match('/^[a-zA-Z 0-9_-]{2,50}$/', $artist->getName())){

		?>
			<br> <br>
			<p style="margin-top: 21px;"> Erro!!!\nPor favor, digite seu nome corretamente!<p>

		 <?php

		showForm($artist);
		
	} else if($artist->getCodCategory() == "none"){

		?>
			<br> <br>
			<p style="margin-top: 21px;"> Selecione uma categoria!!!<p>

		<?php

		showForm($artist);

	} else if ($artist->getPass() != $pass2){

		?>
			<br> <br>
			<p style="margin-top: 21px;"> As senhas não conferem! Por favor, digite as duas senhas iguais e tente novamente!<p>

		<?php

		showForm($artist);

	} else if(!preg_match("/^\w{6,}$/", $artist->getPass())){

		?>
			<br> <br>
			<p style="margin-top: 21px;"> Erro na validação da senha! Por favor, digite uma senha de no mínimo 6 dígitos e sem caracteres especiais.<p>

		<?php

		showForm($artist);

	} else {

  		$temp = $_FILES['profilePicture']['tmp_name'];
  	
  		move_uploaded_file($temp, './upload/users_pictures/'.$artist->getProfilePicture());

		editProfile($con, $artist);	

		?> 

			<h3> Alterado com sucesso!!! Faça agora o seu login! </h3>

			<br>

			<a href="../index.php"> Voltar </a>

		<?php
	}

?>