<?php 

	require('../view/signUp.php');
	require('../model/dbConnection.php');

	$artist = new Artist();

	$artist->setUser($_POST['user']);
	$artist->setName($_POST['name']);
	$artist->setEmail($_POST['email']);
	$artist->setCodCategory($_POST['codCategory']);
	$artist->setProfilePicture($_FILES['profilePicture']['name']);
	$artist->setChannel($_POST['channel']);
	$artist->setBirth($_POST['birth']);
	$artist->setBio($_POST['bio']);
	$artist->setPass($_POST['pass']);
	$pass2 = $_POST['pass2'];

	if(verifyUser($con, $artist->getUser()) == false){

		$artist->setUser("");

		?>
			<br> <br>
			<p style="margin-top: 21px;"> Erro!!! \nEsse usuário já existe!\nPor favor, digite outro usuário e tente novamente!<p>

		 <?php

		showForm($artist);

	} else if(!preg_match("/^\w{4,}$/", $artist->getUser())){

		?>
			<br> <br>
			<p style="margin-top: 21px;"> Erro!!!\nPor favor, digite seu usuário corretamente!<p>

		 <?php

		showForm($artist);

	} else if(!preg_match('/^[a-zA-Z 0-9_-]{2,50}$/', $artist->getName())){

		echo "\n\nErro!!!\nPor favor, digite seu nome corretamente!";
		showForm($artist);
		
	} else if (verifyEmail($con, $artist->getEmail()) == false){

		$artist->setEmail("");

		echo "\n\nErro!!! \nEsse e-mail já sendo utilizado por outro usuário!\n Por favor, digite outro e-mail e tente novamente!";
		showForm($artist);

	} else if(!preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $artist->getEmail())){

		echo "\n\nErro!!!\nPor favor, digite um e-mail um válido!";
		showForm($artist);
	
	} else if($artist->getCodCategory() == "none"){

		echo "\n\nSelecione uma categoria!!!";
		showForm($artist);

	} else if ($artist->getPass() != $pass2){

		echo "\n\nAs senhas não conferem!\n Por favor, digite as duas senhas iguais e tente novamente!";
		showForm($artist);

	} else if(!preg_match("/^\w{6,}$/", $artist->getPass())){

		echo "\n\nErro na validação da senha!\nPor favor, digite uma senha de no mínimo 6 dígitos e sem caracteres especiais.";
		showForm($artist);

	} else {

  		$temp = $_FILES['profilePicture']['tmp_name'];
  	
  		move_uploaded_file($temp, './upload/users_pictures/'.$artist->getProfilePicture());

		signUp($con, $artist);	

		?> 

			<h3> Cadastrado com sucesso!!! Faça agora o seu login! </h3>

			<br>

			<a href="../index.php"> Voltar </a>

		<?php
	}

?>