<?php

class Artist{

	private $user;
	private $name;	
	private $email;
	private $codCategory;
	private $profilePicture;	
	private $coverPicture;
	private $channel;
	private $birth;
	private $bio;
	private $pass;

	function setUser($user){

		$this->user = $user;

	}

	function getUser(){

		return $this->user;

	}

	function setName($name){

		$this->name = $name;

	}

	function getName(){

		return $this->name;

	}

	function setEmail($email){

		$this->email = $email;

	}

	function getEmail(){

		return $this->email;

	}

	function setCodCategory($codCategory){

		$this->codCategory = $codCategory;

	}

	function getCodCategory(){

		return $this->codCategory;

	}

	function setProfilePicture($profilePicture){

		$this->profilePicture = $profilePicture;

	}

	function getProfilePicture(){

		return $this->profilePicture;

	}

	function setCoverPicture($coverPicture){

		$this->coverPicture = $coverPicture;

	}

	function getCoverPicture(){

		return $this->coverPicture;

	}

	function setChannel($channel){

		$this->channel = $channel;

	}

	function getChannel(){

		return $this->channel;

	}

	function setBirth($birth){

		$this->birth = $birth;

	}

	function getBirth(){

		return $this->birth;

	}

	function setBio($bio){

		$this->bio = $bio;

	}

	function getBio(){

		return $this->bio;

	}

	function setPass($pass){

		$this->pass = $pass;

	}

	function getPass(){

		return $this->pass;

	}

}

?>